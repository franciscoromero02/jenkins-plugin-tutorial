package io.jenkins.plugins.sample.actions;

import hudson.FilePath;
import hudson.Launcher;
import hudson.Extension;
import hudson.model.*;
import hudson.util.FormValidation;
import hudson.tasks.Builder;
import hudson.tasks.BuildStepDescriptor;
import io.jenkins.plugins.sample.SampleConfiguration;
import jenkins.model.GlobalConfiguration;
import jenkins.tasks.SimpleBuildStep;
import net.sf.json.JSONObject;
import org.jenkinsci.Symbol;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.QueryParameter;
import io.jenkins.plugins.sample.Messages;

import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Sample {@link Builder}.
 *
 * <p>
 * When the user configures the project and enables this builder,
 * {@link DescriptorImpl#newInstance(StaplerRequest)} is invoked
 * and a new {@link HelloWorldBuilder} is created. The created
 * instance is persisted to the project configuration XML by using
 * XStream, so this allows you to use instance fields (like {@link #name})
 * to remember the configuration.
 *
 * <p>
 * When a build is performed, the {@link #perform(AbstractBuild, Launcher, BuildListener)}
 * method will be invoked.
 *
 * @author Kohsuke Kawaguchi
 */
public class HelloWorldBuilder extends Builder implements SimpleBuildStep {

    public String name;
    public String field;

    // Fields in config.jelly must match the parameter names in the "DataBoundConstructor"
    @DataBoundConstructor
    public HelloWorldBuilder(String name, String field) {
        this.name = name;
        this.field = field;
    }

    /**
     * We'll use this from the <tt>config.jelly</tt>.
     */
    public String getName() {
        return name;
    }

    @DataBoundSetter
    public void setName(String name) { this.name = name; }

    public String getField() { return field; }

    @DataBoundSetter
    public void setField(String field) { this.field = field; }

    @Override
    public boolean perform(AbstractBuild build, Launcher launcher, BuildListener listener) {
        GlobalConfiguration localGlobalCopy = GlobalConfiguration.all().get(SampleConfiguration.class);
        try {
            listener.getLogger().println("\nenv vars keys: " + build.getEnvironment(launcher.getListener()).keySet());
            listener.getLogger().println("\nenv vars values: " + build.getEnvironment(launcher.getListener()).values());
            listener.getLogger().println("\nupstream keys: " + build.getUpstreamBuilds().keySet());
            listener.getLogger().println("\nupstream values" + build.getUpstreamBuilds().values());
            listener.getLogger().println("\nuse french: " + ((SampleConfiguration) localGlobalCopy).getUseFrench());
            listener.getLogger().println("\nDescriptor name: " + getDescriptor().getName2());

        } catch (IOException e) {
            listener.getLogger().println("IO exception: " + e.getMessage());
        } catch (InterruptedException e) {
            listener.getLogger().println("Interruption: " + e.getMessage());
        }
        listener.getLogger().println("is unix: " + launcher.isUnix());
        listener.getLogger().println("URL: " + build.getUrl());
        // This is where you 'build' the project.
        // Since this is a dummy, we just say 'hello world' and call that a build.

        // This also shows how you can consult the global configuration of the builder
        if (getDescriptor().getUseFrench())
            listener.getLogger().println("Bonjour, "+name+"!");
        else
            listener.getLogger().println("Hello, "+name+"!");
        return true;
    }

    // Overridden for better type safety.
    // If your plugin doesn't really define any property on Descriptor,
    // you don't have to do this.
    @Override
    public DescriptorImpl getDescriptor() {
        return (DescriptorImpl)super.getDescriptor();
    }

    @Override
    public void perform(@Nonnull Run<?, ?> run, @Nonnull FilePath filePath, @Nonnull Launcher launcher, @Nonnull TaskListener taskListener) throws InterruptedException, IOException {
        taskListener.getLogger().println("implementing SimpleBuildStep");
    }

    /**
     * Descriptor for {@link HelloWorldBuilder}. Used as a singleton.
     * The class is marked as public so that it can be accessed from views.
     *
     * <p>
     * See <tt>src/main/resources/hudson/plugins/hello_world/HelloWorldBuilder/*.jelly</tt>
     * for the actual HTML fragment for the configuration screen.
     */
    @Symbol("hello plugin")
    @Extension // This indicates to Jenkins that this is an implementation of an extension point.
    public static final class DescriptorImpl extends BuildStepDescriptor<Builder> {
        /**
         * To persist global configuration information,
         * simply store it in a field and call save().
         *
         * <p>
         * If you don't want fields to be persisted, use <tt>transient</tt>.
         */
        private String default3 = Messages.menu3();
        private boolean useFrench;
        private String name2 = Messages.menu_title();
        /**
         * In order to load the persisted global configuration, you have to
         * call load() in the constructor.
         */
        public DescriptorImpl() {
            load();
        }

        /**
         * Performs on-the-fly validation of the form field 'name'.
         *
         * @param value
         *      This parameter receives the value that the user has typed.
         * @return
         *      Indicates the outcome of the validation. This is sent to the browser.
         *      <p>
         *      Note that returning {@link FormValidation#error(String)} does not
         *      prevent the form from being saved. It just means that a message
         *      will be displayed to the user.
         */
        public FormValidation doCheckName(@QueryParameter String value)
                throws IOException, ServletException {
            if (value.length() == 0)
                return FormValidation.error("Please set a name");
            if (value.length() < 4)
                return FormValidation.warning("Isn't the name too short?");
            return FormValidation.ok();
        }

        public boolean isApplicable(Class<? extends AbstractProject> aClass) {
            // Indicates that this builder can be used with all kinds of project types
            return true;
        }

        /**
         * This human readable name is used in the configuration screen.
         */
        public String getDisplayName() {
            return Messages.menu3();
        }

        @Override
        public boolean configure(StaplerRequest req, JSONObject formData) throws FormException {
            // To persist global configuration information,
            // set that to properties and call save().
            useFrench = formData.getBoolean("useFrench");
            // ^Can also use req.bindJSON(this, formData);
            //  (easier when there are many fields; need set* methods for this, like setUseFrench)
            save();
            return super.configure(req,formData);
        }

        /**
         * This method returns true if the global configuration says we should speak French.
         *
         * The method name is bit awkward because global.jelly calls this method to determine
         * the initial state of the checkbox by the naming convention.
         */
        public boolean getUseFrench() {
            return useFrench;
        }

        public String getName2() {
            return name2;
        }

        public String getDefault2() { return default3; }

        public FormValidation doCheckField(@QueryParameter String value)
                throws IOException, ServletException {
            String message = new String();
            Pattern pattern = Pattern.compile("iCART[0-9]{6}");
            Matcher matcher = pattern.matcher(value);
            if(!matcher.find())
                return FormValidation.error("Please insert a valid icart number");

            if (value.length() == 10)
                return FormValidation.error("Please set a name!!!");
            if (value.length() < 9)
                return FormValidation.warning("Isn't the name too short?!!!");
            return FormValidation.ok();
        }
//        @DataBoundSetter
//        public void setUseFrench(boolean useFrench) { this.useFrench = useFrench;}
    }
}
